#!/bin/bash

#To be run in voice_directory/test/xxx/ directory

paste ../../filters/h{1,2,3,4,5}.txt | $ESTDIR/bin/ch_track -s 0.005 -itype ascii -c 0,1,2,3,4 -otype est_ascii -o filters.trk
FILT="'filters.trk"

CG_TMP=jnk_$$
OTYPE=riff
for i in *.mcep
do
    #Synthesizing mixed excitation residual
    filenm=`basename $i .mcep`
    echo "Synthesizing $filenm"
    $ESTDIR/bin/ch_track $i -c 35,36,37,38,39 -otype est_ascii -o $CG_TMP.str
    $ESTDIR/bin/ch_track $i -c 0 -otype est_ascii -o $CG_TMP.f0
    $ESTDIR/bin/ch_track $i -c 1 -otype est_ascii -o $CG_TMP.pow
    # echo "Warning: Using other power coefficients"
    # $ESTDIR/bin/ch_track /home/pmuthuku/voices/me_dummy/cmu_us_rms/test/xxx3/$i -c 1 -otype est_ascii -o $CG_TMP.pow
    STR="'$CG_TMP.str"
    F0="'$CG_TMP.f0"
    POW="'$CG_TMP.pow"
# Couldn't get the following command to work so the line after that is a hack
#    $ESTDIR/../festival/bin/festival -b '(set! filt (track.load '$FILT'))(set! str (track.load '$STR'))(set! f0 (track.load '$F0'))(set! wave (me_residual_synth f0 filt str))(wave.save wave "'${filenm}_res.wav'" "'$OTYPE'")'
    echo "(set! filt (track.load $FILT))
(set! str (track.load $STR))
(set! f0 (track.load $F0))
(set! pow (track.load $POW))
(set! wave (me_residual_synth f0 filt str pow))
(wave.save wave \"${filenm}_res.wav\" \"$OTYPE\")" > dummy.scm
    $ESTDIR/../festival/bin/festival -b dummy.scm
    $ESTDIR/bin/ch_wave ${filenm}_res.wav -scaleN 0.25 -o 1_res.wav
    rm $CG_TMP.*

    #Synthesizing with LSPs
    $ESTDIR/bin/ch_track -otype ascii $i | perl -lane 'if($F[1] >= 0.001){print "@F[1..17]";}' | x2x +a +f > $filenm.lsp
    lsp2lpc -i 0 -m 16 -s 16 $filenm.lsp | x2x +f +a | awk '{if (NR%17==0){print $0}else{printf("%f ",$1);}}' > $filenm.lpc
    awk '{print $1}' $filenm.lpc | perl -lane '$F[0] = exp($F[0]*4); print "$F[0]";'> $filenm.lpcp
    perl -ane '
for($j=1; $j < @F; $j++){                                                          
 $x= - $F[$j];                                                                    
 print "$x ";                                                                      
}                                                                                  
print "\n";' < $filenm.lpc > $filenm.lpcc
    paste $filenm.lpcp $filenm.lpcc > $filenm.lpcr
    $ESTDIR/bin/ch_track -itype  ascii -s 0.005 -c 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 -otype est_ascii $filenm.lpcr > $filenm.lpcs
    ~/fest_est/Feb12/flite/testsuite/lpc_resynth -res 1_res.wav -order 16 $filenm.lpcs ${filenm}.wav
    # ~/fest_est/Feb12/flite/testsuite/lpc_resynth -res ~/voices/me_dummy/cmu_us_rms/test/xxx2/$filenm.wav -order 16 $filenm.lpcs ${filenm}.wav

done
rm *.l*
#rm *_res.wav
#rm filters.trk
rm dummy.scm