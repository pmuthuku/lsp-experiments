#!/bin/bash

#To be run in the voice directory
#Assumes that SPTK binaries are in ~/bin/
if [ ! -d lsp ]
then
    mkdir lsp
fi

cd wav/

for i in *.wav
do
    filenm=`basename $i .wav`
    wav2raw +f $i
    echo "Extracting Line Spectral Pairs for $filenm"
    frame -l 80 -p 80 $filenm.raw | window -l 80 | ~/bin/lpc -l 80 -m 16 | lpc2lsp -m 16 -s 16-o 1 | x2x +f +a | awk '{if (NR%17==0){print $0}else{printf("%f ",$1);}}'> ../lsp/$filenm.lsp
done
rm *.raw

