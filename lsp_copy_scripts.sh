#!/bin/bash

#Pitch extraction code
for i in arctic_a00?9.wav
do
    filenm=`basename $i .wav`
    echo $filenm
    wav2raw +f $i
    frame -l 80 -p 80 $filenm.raw | window -l 80 > $filenm.wind
    pitch -s 16 -l 80 -t 4.5 $filenm.wind | excite -p 80 > $filenm.res
done
rm *.raw *.wind



#Stuff to run in the lsp directory
for i in arctic_a00?9.lsp
do
    filenm=`basename $i .lsp`
    x2x +a +f $i > $filenm.lspb
    lsp2lpc -i 1 -m 16 -s 16 $filenm.lspb > $filenm.lpc
    cat ../wav/$filenm.res | poledf -m 16 -p 80 $filenm.lpc | x2x +fs > ${filenm}_orig.raw
    $ESTDIR/bin/ch_wave -itype raw -f 16000 -otype riff -o ${filenm}_orig.wav ${filenm}_orig.raw
done
rm *.lspb *.lpc *.raw



#Stuff to run in the test/?? directory
for i in *.mcep
do
    filenm=`basename $i .mcep`
    $ESTDIR/bin/ch_track -otype ascii $i | perl -lane 'print "@F[1..17]";' | x2x +a +f > $filenm.lsp
    lsp2lpc -i 1 -m 16 -s 16 $filenm.lsp > $filenm.lpc
    cat ../../wav/$filenm.res | poledf -m 16 -p 80 $filenm.lpc | x2x +fs > ${filenm}_resynth.raw
    $ESTDIR/bin/ch_wave -itype raw -f 16000 -otype riff -o ${filenm}_resynth.wav ${filenm}_resynth.raw
done
rm *.lsp *.lpc *.raw



#Stuff to run in the test/?? directory with correct residual
for i in *.mcep
do
    filenm=`basename $i .mcep`
    $ESTDIR/bin/ch_track -otype ascii $i | perl -lane 'print "1  @F[2..17]";' |x2x +a +f > $filenm.lsp
    lsp2lpc -i 1 -m 16 -s 16 $filenm.lsp > $filenm.lpc
    #Fill in stuff here later
done

##EST LPC STUFF FROM HERE ON
#Stuff to run in the test/?? directory
for i in *.mcep
do
    filenm=`basename $i .mcep`
    $ESTDIR/bin/ch_track -otype ascii $i | perl -lane 'if($F[1] >= 0.001){print "@F[1..17]";}' | x2x +a +f > $filenm.lsp
    lsp2lpc -i 1 -m 16 -s 16 $filenm.lsp | x2x +f +a | awk '{if (NR%17==0){print $0}else{printf("%f ",$1);}}' > $filenm.lpc
    awk '{print $1/2}' $filenm.lpc > $filenm.lpcp;
    perl -ane '
for($j=1; $j < @F; $j++){                                                          
 $x= - $F[$j];                                                                    
 print "$x ";                                                                      
}                                                                                  
print "\n";' < $filenm.lpc > $filenm.lpcc
    paste $filenm.lpcp $filenm.lpcc > $filenm.lpcr
    $ESTDIR/bin/ch_track -itype  ascii -s 0.005 -c 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 -otype est_ascii $filenm.lpcr > $filenm.lpcs
    ~/fest_est/Feb12/flite/testsuite/lpc_resynth -res ../../lpc/${filenm}.res -order 16 $filenm.lpcs ${filenm}_resynth.wav
done
rm *.l*

#Prediction error computation for arctic_a00?9.mcep
for i in *.mcep
do
    filenm=`basename $i .mcep`
    $ESTDIR/bin/ch_track -otype ascii $i | perl -lane 'print "@F[1..17]";' > $filenm.lspa
    $ESTDIR/bin/ch_track -otype ascii ../../ccoefs/$filenm.mcep | perl -lane 'print "@F[1..17]";' > $filenm.lspb
done
cat *.lspa > all.lspa
cat *.lspb > all.lspb
perl ~/code/error_metrics/rms_error.pl all.lspa all.lspb 2 17
perl ~/code/error_metrics/diff_between_lspcoefs.pl all.lspa 2 17 > all.lspc
perl ~/code/error_metrics/diff_between_lspcoefs.pl all.lspb 2 17 > all.lspd
perl ~/code/error_metrics/rms_error.pl all.lspc all.lspd 1 15
rm *.lspa *.lspb *.lspc *.lspd
