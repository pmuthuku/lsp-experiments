#!/bin/perl

use strict;
use warnings;

if($#ARGV < 0){
    print "\nComputes Deltas from \.lsp files\nUsage:\n1. LSP file\n2. LSP order(not including the power coefficient)\n\n";
    exit;
}

open(Input,"$ARGV[0]");
my $order=$ARGV[1];
my @prev_frame=((0) x ($order+1));
my @next_frame;
my @current_frame;
my @delta=((0) x ($order+1));

my $frame=<Input>;
@current_frame=split(/\s+/,$frame);

while(<Input>){
    chomp;
    @next_frame=split(/\s+/,$_);
    #Modifying this so that it also does delta of the power because of the way mlpg is integrated into Festival
    for(my $i=0;$i <= $order; $i++){
	$delta[$i]=((-0.5)*$prev_frame[$i])+(0.5*$next_frame[$i]);
	$prev_frame[$i]=$current_frame[$i];
	$current_frame[$i]=$next_frame[$i];
#	print "$delta[$i] ";
	printf("%.6f ",$delta[$i]);
    }
    print "\n";
}
for(my $i=0; $i <=$order; $i++){
    $delta[$i]=0.5*$prev_frame[$i];
    printf("%.6f ",$delta[$i]);
}
print "\n";
close(Input);
