#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>

int main(int argc,char **argv){
  FILE *fp1,*fp2;
  int c;
  char *inputfile,*outputfile;
  int order;
  int i,k;
  while((c=getopt(argc,argv,"i:o:r:")) != -1)
    switch(c){
    case 'i':
      inputfile=optarg;
      break;
    case 'o':
      outputfile=optarg;
      break;
    case 'r':
      order=atoi(optarg);
      break;
    }
  float dir_form[order];
  float ref_form[order];
  float temp[order];
  float power;

  fp1=fopen(inputfile,"r");
  fp2=fopen(outputfile,"w");
  while(!feof(fp1)){

    //Reading in direct form filter coefficients
    for(i=0; i <= order; i++){
      if(i > 0){
	fscanf(fp1,"%f",&dir_form[i-1]);
      }
      else{
	fscanf(fp1,"%f",&power);
      }
    }

    //Calculating reflection coefficients (eqns 11.56{a,b} in DSP by Defatta, Lucas, Hodgkiss)
    for(i=(order-1); i >= 0; i--){
      ref_form[i]=dir_form[i];

      for(k=0; k <= (i-1); k++){
	temp[k]=(dir_form[k]-(ref_form[i]*dir_form[i-k-1]))/(1-(ref_form[i]*ref_form[i]));
      }

      for(k=0; k < order; k++)
	dir_form[k]=temp[k];
    }

    //Printing reflection coefficients 
    fprintf(fp2,"%f ",power);
    for(i=0; i < order; i++){
      fprintf(fp2,"%f ",ref_form[i]);
    }
    fprintf(fp2,"\n");
  }
  fclose(fp1);
  fclose(fp2);
  return 0;
}
