#!/bin/bash

#To be run in voice directory and expects LPC to have been already extracted

if [ ! -d lsp ]
then
    mkdir lsp
fi

cd lpc/
for i in *.lpc
do
#Need to change sign on LPC coefficients to fit into the SPTK formulations so need to separate the power coefficient from the LPC coefficients in the track file
filenm=`basename $i .lpc`
#Dividing by 4 because that's the sum of my birth day and month 
$ESTDIR/bin/ch_track $i -c 0 -otype ascii | perl -lane '$F[0] = (log($F[0])/4); print "$F[0]";' > $filenm.lpcp
$ESTDIR/bin/ch_track $i -otype ascii | perl -ane '
for($j=1; $j < @F; $j++){
 $x= - $F[$j];
 print "$x ";
}
print "\n";' > $filenm.lpcc
echo "Computing LSPs for $filenm"
paste $filenm.lpcp $filenm.lpcc | x2x +af | lpc2lsp -m 16 -s 16 -o 0 | x2x +fa | awk '{if (NR%17==0){print $0}else{printf("%f ",$1);}}' > ../lsp/$filenm.lsp
echo "Computing LSP deltas for $filenm"
perl /home/pmuthuku/code/lsp_experiments/lsp_deltas.pl ../lsp/$filenm.lsp 16 > ../lsp/$filenm.lspd
paste $filenm.lpcp $filenm.lpcc > $filenm.lpcpc
echo "Computing reflection coefficients for $filenm"
~/code/lsp_experiments/lpc2ref -i $filenm.lpcpc -o jnk -r 16
head -n -1 jnk > ../lsp/$filenm.ref #Have to do this step because of a silly bug that causes an extra line to be generated at the end of the file

done
rm *.lpcp *.lpcc *.lpcpc jnk
cd ..